<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\Role;
use DataTables;
use Illuminate\Support\Facades\Validator;

class RolesController extends Controller
{
    public function index(Request $request){
        $data['roles'] = Role::all();
        $data['title_page'] = 'Roles';
        return view('admin/roles', $data);
    }

    public function create(Request $request){
        //
    }

    public function store(Request $request){

        $validator = Validator::make($request->all(), [
            'name' => "required|min:5",
        ]);

        $increment = Role::max('id');

        $data = [
            'id'    => $increment+1,
            'name'  => $request['name']
        ];
        if ($validator->fails())
        {
            $data['status'] = 'failed';
            $data['message'] = $validator->errors()->all();
            
        }else{
            if(Role::create($data)){
                $data['status'] = 'success';
                $data['message'] = 'Data berhasil Ditambahkan';
            }else{
                $data['status'] = 'failed';
                $data['message'] = 'Something Went Wrong';
            }
        }
        
        return response()->json($data);
        
    }

    public function show(Request $request){
        //
    }

    public function edit($id){
        $role = Role::find($id);
        $role->secret = encrypt($role->id);

        return $role;
    }

    public function update(Request $request, $id){
        $id = decrypt($request['secret']);

        $validator = Validator::make($request->all(), [
            'name' => "required|min:5",
        ]);
        if($validator->fails()){
            $data['status'] = 'failed';
            $data['message'] = $validator->errors()->all();
        }else{
            $role = Role::find($id);
            $role->name = $request['name'];
            if($role->update()){
                $data['status'] = 'success';
                $data['message'] = 'Data berhasil diupdate';
            }else{
                $data['status'] = 'failed';
                $data['message'] = 'Data gagal diupdate';
            }
        }
        return response()->json($data);
    }

    public function destroy($id){
        if(Role::destroy($id)){
            $data['status'] = 'success';
            $data['message'] = 'Data berhasil dihapus';
        }else{
            $data['status'] = 'failed';
            $data['message'] = 'Data gagal dihapus';
        }
        return response()->json($data);
    }

    

    public function ajax_roles(){
        $roles =Role::all();
        return Datatables::of($roles)
        ->addColumn('action',function($roles){
            if($roles->id == 1){
                $disabled = "disabled";
            }else{
                $disabled = "";
            }
            return '<button onclick="edit('.$roles->id.')" type="button" class="btn btn-info btn-xs" '.$disabled.'><i class="fa fa-edit"></i> Edit</button>
            <button onclick="del('.$roles->id.')" type="button" class="btn btn-danger btn-xs" '.$disabled.'><i class="fa fa-trash"></i> Delete</button>';
        })
        ->make(true);
    }

}
