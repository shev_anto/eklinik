<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function dashboard(Request $request){
		$data = array(
			'title_page' => 'Dashboard',
		);
		
		return view('admin/dashboard', $data);
	}
}
