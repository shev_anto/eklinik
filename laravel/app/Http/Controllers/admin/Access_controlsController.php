<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\access_control as Access;
use App\model\Role;
use App\model\Menu;

class Access_controlsController extends Controller
{
    public function index(Request $request){
        $data['roles'] = Role::all();
        $data['title_page'] = 'Access Controls';
        return view('admin/access', $data);
    }

    public function ajax_access(Request $request){
        $role = $request['role'];
        
        $data['access'] = Access::where('role_id',$role)->get();
        //dd($data['access']);
        
        return view('admin/treeview', $data);
    }

    public function generate_menu(){
        $roles = Role::all();
        $menus = Menu::all();
        foreach($roles as $role){
            foreach($menus as $menu){
                Access::firstOrCreate(
                    ['role_id'=> $role->id, 'menu_id'=> $menu->id],
                    ['c'=>'n', 'r'=>'n', 'u'=>'n', 'd'=>'n']
                );
            }
        }
    }
}
