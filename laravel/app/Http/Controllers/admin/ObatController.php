<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\Obat;
use App\User;
use Session;
use DB;
use Auth;
use Response;
use DataTables;

class ObatController extends Controller
{
    public function __construct() {
    	$this->middleware('auth');
    }

    public function index() {
    	$data = array(
    		'title_page' => 'Data Obat'
    	);

    	$obat = Obat::orderBy('id', 'desc')->paginate(10);
        return view('admin/obat/data_obat', compact('obat'), $data,$obat);
    }

    public function create() {
		return view('admin/obat/add_obat');
	}

	/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
	public function store(Request $request) {

    /*$count = Pasien::where('nik',$request->input('nik'))->count();

        if($count>0){
            Session::flash('message', 'Data NIK pasien sudah ada!');
            Session::flash('message_type', 'danger');
            return redirect()->route('admin.index');
        }*/
		$this->validate($request, [
			'kode' => 'required',
			'nama'  => 'required',
			'jenis' => 'required',
			'satuan' => 'required',
			'supplier' => 'required',
			'stock' => 'required',
		]);

		$data = Obat::create([
			'kode_obat' => $request->input('kode'),
			'nama_obat' => $request->input('nama'),
			'jenis_obat' => $request->input('jenis'),
			'satuan' => $request->input('satuan'),
			'supplier' => $request->input('supplier'),
			'stock' => $request->input('stock')
		]);

		Session::flash('message', 'Data obat berhasil ditambahkan!');
        Session::flash('message_type', 'success');
        return Response::json($data);
	}

	public function show($id)
    {
		$data = array(
			'title_page' => 'Data obat'
		);

		$obat = Obat::findOrFail($id);
        return view('admin/obat/data_obat', compact('obat'), $data,$obat);
    }

     public function edit($id)
    {
        $data = array(
            'title_page' => 'Data obat'
        );
        if((Auth::user()->role != '1') && (Auth::user()->id != $id)) {
                Alert::info('Oopss..', 'Anda dilarang masuk ke area ini.');
                return redirect()->route('admin.obat');
        }

        $where = array('id' => $id);
        $obat = Obat::where($where)->first();
        return Response::json($obat);
//        $obat = Obat::findOrFail($id);
//        return view('admin/obat/edit_obat', compact('obat'), $data, $obat);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $_data = Obat::findOrFail($id);
        $_data->kode_obat = $request->input('kode');
        $_data->nama_obat = $request->input('nama');
        $_data->jenis_obat = $request->input('jenis');
        $_data->satuan = $request->input('satuan');
        $_data->supplier = $request->input('supplier');
        $_data->stock = $request->input('stock');

        $_data->update();

        Session::flash('message', 'Berhasil diubah!');
        Session::flash('message_type', 'success');
        return Response::json($_data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $_data = Obat::where('id', $id)->delete();
        Session::flash('message', 'Data obat berhasil dihapus!');
        Session::flash('message_type', 'success');
        return Response::json($_data);
    }

    public function ajax_obat(){
        $obat =Obat::all();
        return Datatables::of($obat)
        ->addColumn('action',function($obat){
           
            return '<button type="button" id="edit-obat" data-id="' . $obat->id . '" class="btn btn-info btn-xs">Edit</button>
            <button type="button" id="delete-obat" data-id="' . $obat->id . '" class="btn btn-danger btn-xs delete-obat">Hapus</button>';
        })
        ->make(true);
    }
}
