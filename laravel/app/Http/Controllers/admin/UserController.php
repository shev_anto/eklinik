<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\model\Role;
use DataTables;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index(Request $request){
        $data['title_page'] = 'User';
        $data['roles']  = Role::all();
        return view('admin/user', $data);
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => "required|min:5",
            'email'=> 'required|email|unique:users',
            'role' => 'required|numeric|not_in:0'
        ]);

        $data = [
            'name'  => $request['name'],
            'email'  => $request['email'],
            'role'  => $request['role'],
            'password' => Hash::make('123456')
        ];

        if ($validator->fails())
        {
            $res['status'] = 'failed';
            $res['message'] = $validator->errors()->all();
        }else{
            if(User::create($data)){
                $res['status'] = 'success';
                $res['message'] = 'Data berhasil Ditambahkan';
            }else{
                $res['status'] = 'failed';
                $res['message'] = 'Something Went Wrong';
            }
        }
        return response()->json($res);

    }

    public function edit($id){
        $user = User::find($id);
        $user->secret = encrypt($user->id);

        return $user;
    }

    public function update(Request $request, $id){
        $id = decrypt($request['secret']);

        $validator = Validator::make($request->all(), [
            'name' => "required|min:5",
            'role' => 'required|numeric|not_in:0'
        ]);
        if($validator->fails()){
            $data['status'] = 'failed';
            $data['message'] = $validator->errors()->all();
        }else{
            $user = User::find($id);
            $user->name = $request['name'];
            $user->role = $request['role'];
            if($user->update()){
                $data['status'] = 'success';
                $data['message'] = 'Data berhasil diupdate';
            }else{
                $data['status'] = 'failed';
                $data['message'] = 'Data gagal diupdate';
            }
        }
        return response()->json($data);
    }

    public function destroy($id){
        if(User::destroy($id)){
            $data['status'] = 'success';
            $data['message'] = 'Data berhasil dihapus';
        }else{
            $data['status'] = 'failed';
            $data['message'] = 'Data gagal dihapus';
        }
        return response()->json($data);
    }

    public function ajax_user(){
        $user =User::all();
        
       return Datatables::of($user)
        ->addColumn('action',function($user){

            if($user->role == 1){
                $disabled = "disabled";
            }else{
                $disabled = "";
            }
           
            return '<button onclick="edit('.$user->id.')" type="button" class="btn btn-info btn-xs" '.$disabled.'><i class="fa fa-edit"></i> Edit</button>
            <button onclick="del('.$user->id.')" type="button" class="btn btn-danger btn-xs" '.$disabled.'><i class="fa fa-trash"></i> Delete</button>';
        })
        ->addColumn('role_name', function($user){
            return $user->roles->name;
        })
        ->make(true); 
    }
}
