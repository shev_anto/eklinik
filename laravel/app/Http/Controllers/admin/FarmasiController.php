<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\Farmasi;
use App\User;
use Session;
use DB;
use Auth;

class FarmasiController extends Controller
{
    public function __construct() {
    	$this->middleware('auth');
    }

    public function index() {
    	$data = array(
    		'title_page' => 'Data farmasi'
    	);

    	$farmasi = farmasi::get();
        return view('admin/farmasi/data_farmasi', compact('farmasi'), $data,$farmasi);
    }

    public function create() {
		return view('admin/farmasi/add_farmasi');
	}

	/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
	public function store(Request $request) {

		$this->validate($request, [
			'kode' => 'required',
			'nama'  => 'required',
			'jenis' => 'required',
			'satuan' => 'required',
			'supplier' => 'required',
			'stock' => 'required',
		]);

		farmasi::create([
			'kode_obat' => $request->input('name'),
			'nama_obat' => $request->input('nik'),
			'type' => $request->input('email'),
			'number_phone' => $request->input('number'),
			'gender' => $request->input('gender'),
			'address' => $request->input('address')
		]);

		Session::flash('message', 'Data farmasi berhasil ditambahkan!');
        Session::flash('message_type', 'success');
        return redirect()->route('admin.farmasi');
	}

	public function show($id)
    {
		$data = array(
			'title_page' => 'Data farmasi'
		);

		$farmasi = farmasi::findOrFail($id);
        return view('admin/farmasi/data_farmasi', compact('farmasi'), $data,$farmasi);
    }

     public function edit($id)
    {
        $data = array(
            'title_page' => 'Data farmasi'
        );
        if((Auth::user()->role != '1') && (Auth::user()->id != $id)) {
                Alert::info('Oopss..', 'Anda dilarang masuk ke area ini.');
                return redirect()->route('admin.farmasi');
        }

        $farmasi = farmasi::findOrFail($id);
        return view('admin/farmasi/edit_farmasi', compact('farmasi'), $data, $farmasi);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $_data = farmasi::findOrFail($id);
        //print_r($_data);die();
        $_data->name_farmasi = $request->input('name');
        $_data->nik = $request->input('nik');
        $_data->email = $request->input('email');
        $_data->number_phone = $request->input('number');
        $_data->gender = $request->input('gender');
        $_data->address = $request->input('address');

        $_data->update();

        Session::flash('message', 'Berhasil diubah!');
        Session::flash('message_type', 'success');
        return redirect()->route('admin.farmasi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
            $_data = farmasi::findOrFail($id);
            $_data->delete();
            Session::flash('message', 'Data farmasi berhasil dihapus!');
            Session::flash('message_type', 'success');

        return redirect()->route('admin.farmasi');
    }
}
