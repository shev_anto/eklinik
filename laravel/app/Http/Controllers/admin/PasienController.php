<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\Pasien;
use App\User;
use Session;
use DB;
use Auth;
use Response;
use DataTables;

class PasienController extends Controller
{
    public function __construct() {
    	$this->middleware('auth');
    }

    public function index() {
    	$data = array(
    		'title_page' => 'Data Pasien'
    	);
      $pasien = Pasien::orderBy('id', 'desc')->paginate(10);
      return view('admin/pasien/data_pasien', compact('pasien'), $data,$pasien);
    }


    public function create() {
		return view('admin/pasien/add_pasien');
	}

	/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
	public function store(Request $request) {

    $count = Pasien::where('nik',$request->input('nik'))->count();

        if($count>0){
            Session::flash('message', 'Data NIK pasien sudah ada!');
            Session::flash('message_type', 'danger');
            return redirect()->route('admin.index');
        }
		$this->validate($request, [
      'name' => 'required',
			'nik'  => 'required',
			'email' => 'required',
			'address' => 'required',
			'number' => 'required',
			'gender' => 'required',
		]);

		$data = Pasien::create([
      'nama_pasien' => $request->input('name'),
			'nik' => $request->input('nik'),
			'email' => $request->input('email'),
      'gender' => $request->input('gender'),
      'number_phone' => $request->input('number'),
			'address' => $request->input('address')
		]);

		Session::flash('message', 'Data pasien berhasil ditambahkan!');
        Session::flash('message_type', 'success');
        return Response::json($data);
	}

	public function show($id)
    {
		$data = array(
			'title_page' => 'Data Pasien'
		);

		$pasien = Pasien::findOrFail($id);
        return view('admin/pasien/data_pasien', compact('pasien'), $data,$pasien);
    }

     public function edit($id)
    {
        $data = array(
            'title_page' => 'Data Pasien'
        );
        if((Auth::user()->role != '1') && (Auth::user()->id != $id)) {
                Alert::info('Oopss..', 'Anda dilarang masuk ke area ini.');
                return redirect()->route('admin.pasien');
        }
        $where = array('id' => $id);
        $pasien = Pasien::where($where)->first();
        return Response::json($pasien);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $_data = Pasien::findOrFail($id);
        $_data->nama_pasien = $request->input('name');
        $_data->nik = $request->input('nik');
        $_data->email = $request->input('email');
        $_data->number_phone = $request->input('number');
        $_data->gender = $request->input('gender');
        $_data->address = $request->input('address');

        $_data->update();

        Session::flash('message', 'Berhasil diubah!');
        Session::flash('message_type', 'success');
        return Response::json($_data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $_data = Pasien::where('id', $id)->delete();
            Session::flash('message', 'Data pasien berhasil dihapus!');
            Session::flash('message_type', 'success');
        return Response::json($_data);
    }

    public function ajax_pasien(){
        $pasien =Pasien::all();
        return Datatables::of($pasien)
        ->addColumn('action',function($pasien){
            
            return '<button type="button" id="edit-pasien" data-id="' . $pasien->id . '" class="btn btn-info btn-xs">Edit</button>
            <button type="button" id="delete-pasien" data-id="' . $pasien->id . '" class="btn btn-danger btn-xs delete-pasien">Hapus</button>';
        })
        ->make(true);
    }
}
