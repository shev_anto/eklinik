<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Obat extends Model
{
  protected $table = 'obat';
  protected $fillable = [
     'kode_obat', 'nama_obat', 'jenis_obat', 'satuan', 'supplier','stock'
 ];
}
