<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class menu extends Model
{
    protected $table = 'menus';
/* 
    public function children() {
        return $this->hasMany('menu','parent');
    }

    public function parent() {
        return $this->belongsTo('menu','parent');
    } */

    public function access() {
        return $this->hasOne('App\access_control','id');
    }

}
