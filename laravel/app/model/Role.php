<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = ['id','name'];

    protected $tables = 'roles';
    
    public function users(){
        return $this->hasOne('App\User','id');
    }
}
