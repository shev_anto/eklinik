<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class access_control extends Model
{
    protected $tables = 'access_controls';

    protected $fillable = ['role_id', 'menu_id', 'c', 'r', 'u', 'd'];

    public function menu(){
        return $this->belongsTo('App\model\menu','menu_id');
    }
}
