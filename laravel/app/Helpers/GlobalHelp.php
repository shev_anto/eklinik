<?php
namespace App\Helpers;

use Illuminate\Support\Facades\DB;

class GlobalHelp {

    public static function authorized($role, $menu, $action) {

        if($role == 1){
            return True;
        }else{
            $access = DB::table('access_controls')->where([
                ['role_id', '=', $role],
                ['menu_id', '=', $menu]
                ]);
            if(!empty($access->$action)){
                if($access->$action == 'y'){
                    return True;
                }else{
                    return False;
                }
            }
        }
    }

} // End Global Helper

?>