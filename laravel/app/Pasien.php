<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pasien extends Model
{
     protected $table = 'pasien';
     protected $fillable = [
        'nama_pasien', 'nik', 'email', 'gender', 'number_phone','address'
    ];
}
