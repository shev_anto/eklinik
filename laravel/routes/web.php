<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function(){
    Route::get('dashboard', 'admin\AdminController@dashboard');

    Route::resource('roles', 'admin\RolesController');
    Route::get('ajax_roles','admin\RolesController@ajax_roles')->name('ajax_roles');
    
    Route::resource('users', 'admin\UserController');
    Route::get('ajax_user','admin\UserController@ajax_user')->name('ajax_user');

    Route::get('access_controls', 'admin\Access_controlsController@index')->name('access_control');
    Route::post('ajax_access','admin\Access_controlsController@ajax_access')->name('ajax_access');
    Route::post('generate_menu','admin\Access_controlsController@generate_menu')->name('generate');

    Route::resource('pasien', 'admin\PasienController');
    Route::get('ajax_pasien','admin\PasienController@ajax_pasien')->name('ajax_pasien');

    Route::resource('obat', 'admin\ObatController');
    Route::get('ajax_obat','admin\ObatController@ajax_obat')->name('ajax_obat');

    /* Route::group(['prefix' => 'pasien'], function(){
        Route::get('pasien', 'admin\PasienController@index')->name('pasien.index');
        Route::post('store', 'admin\PasienController@store');
        Route::get('edit/{id}', 'admin\PasienController@edit');
        Route::post('update/{id}', 'admin\PasienController@update');
        Route::post('destroy/{id}', 'admin\PasienController@destroy');
    }); */
/* 
    Route::group(['prefix' => 'obat'], function(){
        Route::get('obat', 'admin\ObatController@index')->name('obat.index');
        Route::post('store', 'admin\ObatController@store');
        Route::get('edit/{id}', 'admin\ObatController@edit');
        Route::post('update/{id}', 'admin\ObatController@update');
        Route::post('destroy/{id}', 'admin\ObatController@destroy');
    }); */
    
});




