<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAccessControl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('access_controls', function (Blueprint $table) {
           $table->integer('role_id');
           $table->integer('menu_id');
           $table->char('c')->default('n');
           $table->char('r')->default('n');
           $table->char('u')->default('n');
           $table->char('d')->default('n');

           $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade')->onUpdate('cascade');
           $table->foreign('menu_id')->references('id')->on('menus')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('access_controls', function (Blueprint $table) {
            $table->dropForeign('access_controls_role_id_foreign');
            $table->dropForeign('access_controls_menu_id_foreign');

            $table->dropColumn('role_id');
            $table->dropColumn('menu_id');
            $table->dropColumn('c')->default('n');
            $table->dropColumn('r')->default('n');
            $table->dropColumn('u')->default('n');
            $table->dropColumn('d')->default('n');
 
         });
    }
}
