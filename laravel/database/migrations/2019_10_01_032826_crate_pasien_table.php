<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CratePasienTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('pasien', function (Blueprint $table) {
          $table->increments('id');
          $table->string('nama_pasien', 35);
          $table->string('nik', 16);
          $table->string('email')->unique();
          $table->string('gender', 2);
          $table->string('number_phone', 12);
          $table->string('address', 200);
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pasien');
    }
}
