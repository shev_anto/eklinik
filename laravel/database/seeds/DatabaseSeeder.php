<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Administrator',
            'email' => 'shev_anto@yahoo.com',
            'password' => bcrypt('123456'),
			'role' => 1,
			'created_at' => date('Y-m-d H:i:s')
        ]);
		
		DB::table('roles')->insert([
			'id' => 1,
            'name' => 'Super Admin',
			'created_at' => date('Y-m-d H:i:s')
        ]);
		
		DB::table('roles')->insert([
			'id' => 2,
            'name' => 'Customer Service',
			'created_at' => date('Y-m-d H:i:s')
        ]);
		
		DB::table('roles')->insert([
			'id' => 3,
            'name' => 'Perawat',
			'created_at' => date('Y-m-d H:i:s')
        ]);
		
		DB::table('roles')->insert([
			'id' => 4,
            'name' => 'Dokter',
			'created_at' => date('Y-m-d H:i:s')
        ]);
		
    }
}
