<?php

use Illuminate\Database\Seeder;

class MenuSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menus')->insert([
            'id'    => 1,
            'parent' => 0,
            'menu_name' => 'Dashboard',
            'link' => 'dashboard',
            'icon' => 'fa fa-home',
            'sorting' => 1
        ]);
        DB::table('menus')->insert([
            'id'    => 2,
            'parent' => 0,
            'menu_name' => 'Administrator',
            'link' => '#',
            'icon' => 'fa fa-user-groups',
            'sorting' => 50
        ]);
        DB::table('menus')->insert([
            'id'    => 3,
            'parent' => 2,
            'menu_name' => 'Role',
            'link' => 'role',
            'icon' => 'none',
            'sorting' => 1
        ]);
        DB::table('menus')->insert([
            'id'    => 4,
            'parent' => 2,
            'menu_name' => 'User Data',
            'link' => 'user',
            'icon' => 'none',
            'sorting' => 2
        ]);
        DB::table('menus')->insert([
            'id'    => 5,
            'parent' => 2,
            'menu_name' => 'Access Controls',
            'link' => 'menu',
            'icon' => 'none',
            'sorting' => 3
        ]);
        DB::table('menus')->insert([
            'id'    => 6,
            'parent' => 0,
            'menu_name' => 'Master',
            'link' => 'master',
            'icon' => 'fa fa-database',
            'sorting' => 5
        ]);
        DB::table('menus')->insert([
            'id'    => 7,
            'parent' => 6,
            'menu_name' => 'Data Pasien',
            'link' => 'pasien',
            'icon' => 'none',
            'sorting' => 1
        ]);
        DB::table('menus')->insert([
            'id'    => 8,
            'parent' => 6,
            'menu_name' => 'Data Obat',
            'link' => 'obat',
            'icon' => 'none',
            'sorting' => 2
        ]);
    }
}
