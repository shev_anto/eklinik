@extends('admin/template')

@section($title_page)

@section('konten')
	<!-- top tiles -->
        <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <a href="{{route('admin.create')}}"><button type="button" class="btn btn-success">Tambah Data Obat</button></a>
                  <div class="col-lg-12">
                  @if (Session::has('message'))
                  <div class="alert alert-{{ Session::get('message_type') }}" id="waktu2" style="margin-top:10px;font-size:14px;">{{ Session::get('message') }}</div>
                  @endif
                  </div>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a href="#"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <p class="text-muted font-13 m-b-30">

                  </p>
                  <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>Kode Obat</th>
                        <th>Nama Obat</th>
                        <th>Jenis Obat</th>
                        <th>Satuan</th>
                        <th>Supplier</th>
                        <th>Stock</th>
                        <th>Action</th>
                      </tr>
                    </thead>


                    <tbody>
                      @foreach($farmasi as $data)
                      <tr>
                        <td>{{$data->kode_obat}}</td>
                        <td>{{$data->nama_obat}}</td>
                        <td>{{$data->type_obat}}</td>
                        <td>{{$data->satuan}}</td>
                        <td>{{$data->supplier}}</td>
                        <td>{{$data->stock}}</td>
                        <td>
                  <div class="btn-group">
                    <button data-toggle="dropdown" class="btn btn-default dropdown-toggle" type="button" aria-expanded="false">Action <span class="caret"></span>
                  </button>
                    <ul class="dropdown-menu" role="menu">
                      <li class="divider"></li>
                      <li><a href="{{ route('admin.edit', $data->id) }}">Edit</a>
                      </li>
                      <li class="divider"></li>
                      <li>

                          <li><a onclick="return confirm('Anda yakin ingin menghapus data ini?')" href="{{ route('admin.delete', $data->id) }}">Hapus</a></li>

                      </li>
                    </ul>
                  </div>
                        </td>
                      </tr>
                      @endforeach

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
@endsection
