@extends('admin/template')

@section('judul_halaman',$title_page)

@section('konten')
<div class="col-md-12">
	<div class="row">
	<div class="panel panel-primary">
		<div class="panel-heading" style="">
			{{$title_page}}
		</div>
		<div class="panel-body">
		<div class="col-md-12"><a id="addForm" type="button" class="btn btn-primary btn-sm"><i class="fa fa-plus-square"></i> Add Data</a><hr style="border-bottom:3px solid orange"></div>
		
		<table id="user-table" class="table table-bordered table-responsive">
			<thead>
			<tr>
				<th>Nama</th>
				<th>Email</th>
				<th>Role</th>
				<th>Status</th>
				<th width="15%">Action</th>
			</tr>
			</thead>
			<tbody>
				
			</tbody>
		</table>
		</div>
	</div>
	</div>
</div>
@endsection

@push('js')
<script>
	var table = $("#user-table").DataTable({
			processing:true,
			serverSide:true,
			ajax:"{{route('ajax_user')}}",
			columns:[
				{data:'name', name:"name"},
				{data:'email', name:"email"},
				{data:'role_name', name:"role_name"},
				{data:'status', name:"status"},
				{data:'action', name:"action", orderable:false, searchable:false},
			]
		})
$(document).ready(function(){
	$("#addForm").click(function(){
		save_method = 'add';
		$('input[name=_method]').val('POST');
		$("#modal form")[0].reset();
		$(".modal-title").html('Add Form')
		$("#modal").modal('show');
		$("#error").empty().css('display','none');
		$("#email").removeAttr('disabled');
	});

	$('#save').click(function(){
		var id = $("#id").val();
		if(save_method == 'add'){
			url = "{{url('admin/users')}}";
		}else{
			url = "{{url('admin/users').'/'}}"+id;
		}
		$.ajax({
			url	: url,
			type:"post",
			data:$('#modal form').serialize(),
			dataType:"JSON",
			success:function(response){
				if(response.status == 'success'){
					Lobibox.notify('success', {
					msg: response.message
					});
					$("#modal").modal('hide');
					table.ajax.reload();
				}else{
					
					$.each(response.message, function(key, value){
						$("#error").css('display','block');
						$("#error").append('<p><i class="fa fa-exclamation"></i> '+value+'</p>');
					})
					
				}
				
			},error:function(response){
				Lobibox.notify('error', {
					msg: 'Please try again later'
					});
			}
		});
	})

});

function edit(id){
		save_method = 'edit';
		$('input[name=_method]').val('PATCH');
		$("#modal form")[0].reset();
		$("#error").empty().css('display','none');
		$("#email").attr('disabled','disabled');
		
		$.ajax({
			url	:"{{url('admin/users').'/'}}"+id+'/edit',
			type:"GET",
			dataType:"JSON",
			success:function(res){
				$("#modal").modal('show');
				$(".modal-title").html('Edit Form');

				$("#id").val(res.id);
				$("#name").val(res.name);
				$("#email").val(res.email);
				$("#role").val(res.role);
				$("#secret").val(res.secret);
			},error:function(){
				Lobibox.notify('error', {
					msg: 'Please try again later'
					});
			}
		})

	}

	function del(id){
		var token = $('input[name=_token]').val();
	 Lobibox.confirm({
		 title: "Konfirmasi",
		 msg: "Anda yakin akan menghapus data ini ?",
		 callback: function ($this, type) {
			if (type === 'yes'){
				$.ajax({
					url :"{{ url('admin/users')}}"+'/'+id,
					type:"post",
					data:{'_method' : 'DELETE', '_token' : token},
					success:function(response){
						if(response.status == 'success'){
							Lobibox.notify('success', {
							msg: response.message
							});
							table.ajax.reload();
						}else{
							Lobibox.notify('error', {
							msg: response.message
							});
						}
					},
					error:function(){
					Lobibox.notify('error', {
					msg: 'Gagal Melakukan Hapus data'
					});
					}
				})
			}
    }
    })
				
}
</script>

<div class="modal fade" tabindex="-1" role="dialog" id="modal">
		<div class="modal-dialog" role="modal">
			<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
					
				<form id="form-role" method="POST" class="form-horizontal" data-toggle="validator">
					{{ csrf_field() }}	{{ method_field('POST') }}
					<input type="hidden" id="id" name="id">
					<input type="hidden" id="secret" name="secret">
					<div class="col-md-12">
						
						<div class="alert alert-danger" id="error" style="display:none">
							
						</div>
					<div class="form-group">
						<div class="col-md-3">
							<label>Fullname</label>
						</div>
						<div class="col-md-9">
							<input type="text" class="form-control" name="name" id="name">
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-3">
							<label>Email</label>
						</div>
						<div class="col-md-9">
							<input type="text" class="form-control" name="email" id="email">
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-3">
							<label>Role</label>
						</div>
						<div class="col-md-9">
							<select name="role" id="role" class="form-control">
								<option value="0">Select Role</option>
								@foreach ($roles as $item)
									<option value="{{$item->id}}">{{$item->name}}</option>
								@endforeach
							</select>
						</div>
					</div>
					
					</div>
				</form>
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
				<button type="button" class="btn btn-primary" id="save"><i class="fa fa-save"></i> Save</button>
			</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
		</div>
@endpush