@extends('admin/template')

@section($title_page)

@section('konten')
	<!-- top tiles -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
        <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <a href="javascript:void(0)" class="btn btn-success mb-2" id="create-new-obat">Tambah Data Obat</a>
                  <!--<div class="col-lg-12">
                  @if (Session::has('message'))
                  <div class="alert alert-{{ Session::get('message_type') }}" id="waktu2" style="margin-top:10px;font-size:14px;">{{ Session::get('message') }}</div>
                  @endif
                </div>-->
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a href="#"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <p class="text-muted font-13 m-b-30">

                  </p>
                  <table id="data-obat" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>Kode Obat</th>
                        <th>Nama Obat</th>
                        <th>Jenis Obat</th>
                        <th>Satuan</th>
                        <th>Supplier</th>
                        <th>Stock</th>
                        <th>Action</th>
                      </tr>
                    </thead>


                    <tbody>
                     
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
@endsection

@push('js')
    
<div class="modal fade" id="ajax-crud-modal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="obatModal"></h4>
        </div>
        <div class="modal-body">
            <form id="obatForm" name="obatForm" class="form-horizontal" novalidate>
               <input type="hidden" name="obat_id" id="obat_id">
               <div class="item form-group">
                 <label class="control-label col-md-3 col-sm-3 col-xs-12" for="kode">Kode Obat <span class="required">*</span>
                 </label>
                 <div class="col-md-9 col-sm-9 col-xs-12">
                   <input id="kode" class="form-control col-md-7 col-xs-12" name="kode" required="required" type="text">
                 </div>
               </div>
               <div class="item form-group">
                 <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Nama Obat <span class="required">*</span>
                 </label>
                 <div class="col-md-9 col-sm-9 col-xs-12">
                   <input type="text" id="nama" name="nama" required="required" class="form-control col-md-7 col-xs-12">
                 </div>
               </div>
               <div class="item form-group">
                 <label class="control-label col-md-3 col-sm-3 col-xs-12">Jenis Obat</label>
                 <div class="col-md-9 col-sm-9 col-xs-12">
                   <select class="form-control" id="jenis" name="jenis" required="required">
                     <option value="bebas">Obat Bebas</option>
                     <option value="resep">Obat Resep</option>
                   </select>
                 </div>
               </div>
               <div class="item form-group">
                 <label class="control-label col-md-3 col-sm-3 col-xs-12">Satiuan</label>
                 <div class="col-md-9 col-sm-9 col-xs-12">
                   <select class="form-control" id="satuan" name="satuan" required="required">
                     <option value="box">Box</option>
                     <option value="strip">Strip</option>
                   </select>
                 </div>
               </div>
               <div class="item form-group">
                 <label class="control-label col-md-3 col-sm-3 col-xs-12" for="supplier">Supplier
                 </label>
                 <div class="col-md-9 col-sm-9 col-xs-12">
                   <input type="text" id="supplier" name="supplier" class="form-control col-md-7 col-xs-12">
                 </div>
               </div>

               <div class="item form-group">
                 <label class="control-label col-md-3 col-sm-3 col-xs-12" for="stock">Stock <span class="required">*</span>
                 </label>
                 <div class="col-md-9 col-sm-9 col-xs-12">
                   <input type="text" id="stock" name="stock" required="required" data-validate-length-range="8,20" class="form-control col-md-7 col-xs-12">
                 </div>
               </div>
            </form>
        </div>
        <div class="modal-footer">
            <button id="btn-save" type="submit" class="btn btn-success">Submit</button>
            <button id="btn-edit" type="submit" class="btn btn-success">Edit</button>
        </div>
    </div>
  </div>
</div>

<!--<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>-->

<script>
  var table = $("#data-obat").DataTable({
			processing:true,
			serverSide:true,
			ajax:"{{route('ajax_obat')}}",
			columns:[
				{data:'kode_obat', name:"kode_obat"},
				{data:'nama_obat', name:"nama_obat"},
        {data:'jenis_obat', name:"jenis_obat"},
        {data:'satuan', name:"satuan"},
        {data:'supplier', name:"supplier"},
        {data:'stock', name:"stock"},
				{data:'action', name:"action", orderable:false, searchable:false},
			]
		})
  $(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    /*  add button obat */
    $('#create-new-obat').click(function () {
        $('#btn-save').val("create-obat");
        $('#obatForm').trigger("reset");
        $('#obatModal').html("Form Obat");
        $('#ajax-crud-modal').modal('show');
        $('#btn-edit').hide();
        $('#btn-save').show();
    });

   /* edit pasien */
    $('body').on('click', '#edit-obat', function () {
      var obat_id = $(this).data('id');
      var url = "{{ url('admin/obat') }}"+'/'+obat_id+'/edit';
      $.get(url, function (data) {
         $('#obatModal').html("Edit Obat");
          $('#btn-edit').val("edit-obat");
          $('#btn-edit').show();
          $('#btn-save').hide();
          $('#ajax-crud-modal').modal('show');
          $('#obat_id').val(data.id);
          $('#kode').val(data.kode_obat);
          $('#nama').val(data.nama_obat);
          $('#jenis').val(data.jenis_obat);
          $('#satuan').val(data.satuan);
          $('#supplier').val(data.supplier);
          $('#stock').val(data.stock);
      })
   });

   //delete obat
    $('body').on('click', '.delete-obat', function () {
        var obat_id = $(this).data("id");
        var confirmText = "Anda yakin ingin menghapus data ini?";
        if(confirm(confirmText)) {
        $.ajax({
            type: "DELETE",
            url: "{{ url('admin/obat')}}"+'/'+obat_id,
            success: function (data) {
                table.ajax.reload();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
        }
      return false;
    });
  });

<!-- ADD OBAT -->
  $('#btn-save').on('click', function() {
        var actionType = $('#btn-save').val();
        $('#btn-save').html('Sending..');
  $.ajax({
      data: $('#obatForm').serialize(),
      url: "{{ url('admin/obat') }}",
      type: "POST",
      dataType: 'json',
      success: function (data) {
          table.ajax.reload();
          $('#ajax-crud-modal').modal('hide');
          $('#btn-save').html('Submit');

      },
      error: function (data) {
          console.log('Error:', data);
          $('#btn-save').html('Submit');
      }
  });
  });

<!-- EDIT PASIEN -->
  $('#btn-edit').on('click', function() {
    var obat_id = $('#obat_id').val();
        var actionType = $('#btn-edit').val();
        $('#btn-edit').html('Sending..');
  $.ajax({
      data: $('#obatForm').serialize(),
      url: "{{ url('admin/obat') }}"+'/'+obat_id,
      type: "PUT",
      dataType: 'json',
      success: function (data) {
          table.ajax.reload();
          $('#ajax-crud-modal').modal('hide');
          $('#btn-edit').html('Edit');


      },
      error: function (data) {
          console.log('Error:', data);
          $('#btn-save').html('Submit');
      }
  });
  });
</script>
@endpush