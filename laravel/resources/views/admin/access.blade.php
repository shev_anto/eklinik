@extends('admin/template')

@section('konten')

<div class="col-md-12">
	<div class="row">
	<div class="panel panel-primary">
		<div class="panel-heading" style="">
			{{$title_page}}
		</div>
		<div class="panel-body">
		<div class="col-md-12"  style="border-bottom:3px solid orange">
			<div class="col-md-3">
				<form id="form">
					{{ csrf_field() }}
				<div class="form-group">
					<select name="role" id="role" class="form-control">
						<option value="0"><strong>Select Role</strong></option>
						@foreach ($roles as $item)
							<option value="{{$item->id}}">{{$item->name}}</option>
						@endforeach
					</select>
				</div>
				</form>

			</div>
			@if(env('DEV_MODE') == 'DEVELOPMENT')
			<div class="col-md-2 pull-right">
				<button type="button" class="btn btn-success pull-righ generate" data-toggle="tooltip" data-placement="top" title="Gunakan Ini saat ada penambahan menu baru (Hanya Untuk Developer)">
					<i class="fa fa-sitemap"> Generate Menu</i>
				</button>
			</div>
			@endif
		</div>
		
		<div class="col-md-12" id="render" style="text-align:center">
			
		</div>
			
		</div>
	</div>
	</div>
</div>

@endsection


@push('js')
<script>
$(document).ready(function(){

	$("#role").change(function(){
		reload_treeview();
	})
	
	var role = null;
	$('.generate').click(function(){
		$.ajax({
			url :"{{url('admin/generate_menu')}}",
			type:"POST",
			data:$("#form").serialize(),
			success:function(){
				Lobibox.notify('success', {
				msg: 'Menu Berhasil di Generate'
				});
			},
			error:function(){
				Lobibox.notify('error', {
				msg: 'Please try again later'
				});
			}

		})
		reload_treeview();
	})
})
function reload_treeview(){
	$.ajax({
		url :"{{url('admin/ajax_access')}}",
		type:"POST",
		data:$("#form").serialize(),
		beforeSend:function(){
			$("#render").html('{{Html::image('/admin/images/waiting.gif')}}');
		},
		success:function(res){
			setTimeout(function() 
				{
					$("#render").html(res);
				}, 500);
			
		},
		error:function(){
			Lobibox.notify('error', {
			msg: 'Please try again later'
			});
		}
	})
}
</script>
@endpush

