@extends('admin/template')

@section($title_page)

@section('konten')
	<!-- top tiles -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
        <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <a href="javascript:void(0)" class="btn btn-success mb-2" id="create-new-pasien">Tambah Data Pasien</a>
                  <!--<div class="col-lg-12">
                  @if (Session::has('message'))
                  <div class="alert alert-{{ Session::get('message_type') }}" id="waktu2" style="margin-top:10px;font-size:14px;">{{ Session::get('message') }}</div>
                  @endif
                </div>-->
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a href="#"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <p class="text-muted font-13 m-b-30">

                  </p>
                  <table id="pasien-table" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                          <th>ID</th>
                        <th>Nama Pasien</th>
                        <th>NIK</th>
                        <th>Jenis Kelamin</th>
                        <th>Alamat</th>
                        <th>No. Handphone</th>
                        <th>Action</th>
                      </tr>
                    </thead>


                    <tbody>
                  
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
@endsection

@push('js')
<div class="modal fade" id="ajax-crud-modal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="pasienModal"></h4>
        </div>
        <div class="modal-body">
            <form id="pasienForm" name="pasienForm" class="form-horizontal" novalidate>
               <input type="hidden" name="pas_id" id="pas_id">
               <div class="item form-group">
                 <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nama Pasien <span class="required">*</span>
                 </label>
                 <div class="col-md-9 col-sm-9 col-xs-12">
                   <input id="name" class="form-control col-md-7 col-xs-12" name="name" required="required" type="text">
                 </div>
               </div>
               <div class="item form-group">
                 <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nik">NIK <span class="required">*</span>
                 </label>
                 <div class="col-md-9 col-sm-9 col-xs-12">
                   <input type="text" id="nik" name="nik" required="required" class="form-control col-md-7 col-xs-12">
                 </div>
               </div>
               <div class="item form-group">
                 <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email
                 </label>
                 <div class="col-md-9 col-sm-9 col-xs-12">
                   <input type="email" id="email" name="email" class="form-control col-md-7 col-xs-12">
                 </div>
               </div>

               <div class="item form-group">
                 <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">No. Handphone <span class="required">*</span>
                 </label>
                 <div class="col-md-9 col-sm-9 col-xs-12">
                   <input type="tel" id="number" name="number" required="required" data-validate-length-range="8,20" class="form-control col-md-7 col-xs-12">
                 </div>
               </div>
               <div class="item form-group">
                 <label class="control-label col-md-3 col-sm-3 col-xs-12">Jenis Kelamin</label>
                 <div class="col-md-9 col-sm-9 col-xs-12">
                   <select class="form-control" id="gender" name="gender" required="required">
                     <option value="L">Laki-laki</option>
                     <option value="P">Perempuan</option>
                   </select>
                 </div>
               </div>
               <div class="item form-group">
                 <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Alamat <span class="required">*</span>
                 </label>
                 <div class="col-md-9 col-sm-9 col-xs-12">
                   <textarea id="address" required="required" name="address" class="form-control col-md-7 col-xs-12"></textarea>
                 </div>
               </div>
            </form>
        </div>
        <div class="modal-footer">
            <button id="btn-save" type="submit" class="btn btn-success">Submit</button>
            <button id="btn-edit" type="submit" class="btn btn-success">Edit</button>
        </div>
    </div>
  </div>
</div>

<!--<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>-->


<script>
  var table = $("#pasien-table").DataTable({
			processing:true,
			serverSide:true,
			ajax:"{{route('ajax_pasien')}}",
			columns:[
				{data:'id', name:"id"},
				{data:'nama_pasien', name:"nama_pasien"},
        {data:'nik', name:"nik"},
        {data:'gender', name:"gender"},
        {data:'address', name:"address"},
        {data:'number_phone', name:"number_phone"},
				{data:'action', name:"action", orderable:false, searchable:false},
			]
		})
  $(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    /*  add button pasien */
    $('#create-new-pasien').click(function () {
        $('#btn-save').val("create-pasien");
        $('#pasienForm').trigger("reset");
        $('#pasienModal').html("Form Pasien");
        $('#ajax-crud-modal').modal('show');
        $('#btn-edit').hide();
        $('#btn-save').show();
    });

   /* edit pasien */
    $('body').on('click', '#edit-pasien', function () {
      var pas_id = $(this).data('id');
      var url = "{{ url('admin/pasien') }}"+'/'+pas_id+'/edit';
      //$.get('ajax-crud/' + user_id +'/edit', function (data) {
      $.get(url, function (data) {
         $('#pasienModal').html("Edit Pasien");
          $('#btn-edit').val("edit-pasien");
          $('#btn-edit').show();
          $('#btn-save').hide();
          $('#ajax-crud-modal').modal('show');
          $('#pas_id').val(data.id);
          $('#name').val(data.nama_pasien);
          $('#nik').val(data.nik);
          $('#email').val(data.email);
          $('#number').val(data.number_phone);
          $('#gender').val(data.gender);
          $('#address').val(data.address);
      })
   });

   //delete pasien
    $('body').on('click', '.delete-pasien', function () {
        var pas_id = $(this).data("id");
        var confirmText = "Anda yakin ingin menghapus data ini?";
        if(confirm(confirmText)) {
        $.ajax({
            type: "DELETE",
            url: "{{ url('admin/pasien')}}"+'/'+pas_id,
            success: function (data) {
                $("#pas_id_" + pas_id).remove();
                table.ajax.reload();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
        }
      return false;
    });
  });

<!-- ADD PASIEN -->
  $('#btn-save').on('click', function() {
        var actionType = $('#btn-save').val();
        $('#btn-save').html('Sending..');
  $.ajax({
      data: $('#pasienForm').serialize(),
      url: "{{ url('admin/pasien') }}",
      type: "POST",
      dataType: 'json',
      success: function (data) {

          table.ajax.reload();
          $('#ajax-crud-modal').modal('hide');
          $('#btn-save').html('Submit');

      },
      error: function (data) {
          console.log('Error:', data);
          $('#btn-save').html('Submit');
      }
  });
  });

<!-- EDIT PASIEN -->
  $('#btn-edit').on('click', function() {
    var pas_id = $('#pas_id').val();
        var actionType = $('#btn-edit').val();
        $('#btn-edit').html('Sending..');
  $.ajax({
      data: $('#pasienForm').serialize(),
      url: "{{ url('admin/pasien') }}"+"/"+pas_id,
      type: "PUT",
      dataType: 'json',
      success: function (data) {
          table.ajax.reload();
          $('#ajax-crud-modal').modal('hide');
          $('#btn-edit').html('Edit');

      },
      error: function (data) {
          console.log('Error:', data);
          $('#btn-save').html('Submit');
      }
  });
  });
</script>
@endpush
